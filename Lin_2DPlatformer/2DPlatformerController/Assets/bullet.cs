﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    // GameOject for perfab gem
    public GameObject Gem;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // when the bullet hits the crate, spawn a gem at crate's position
        if (collision.gameObject.CompareTag("Crate"))
        {
            collision.gameObject.SetActive(false);
            Destroy(gameObject);
            Instantiate(Gem, collision.transform.position, collision.transform.rotation);
        }
        // destroy the bullet when it hits platform level
        if (collision.gameObject.CompareTag("platform"))
        {
            Destroy(gameObject);
        }
    }

}
