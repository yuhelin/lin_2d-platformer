﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weapon : MonoBehaviour
{
    // bullet speed variable
    public float speed;
    // transform to get the position of firepoint
    public Transform firepoint;
    // GameObject for bullet prefab
    public GameObject bulletPrefab;
    // boolean to check if the player has a weapon
    public bool hasWeapon;
    // boolean to check if the sprite of player flip or not
    public bool hasFlip;

    private void Start()
    {
        // set the bullet speed and player is not flip at the beginning of the game
        speed = 10;
        hasFlip = false;
    }

    void Update()
    {
        // if the player has weapon and hit fire, player shoots
        if (Input.GetButtonDown("Fire1") && hasWeapon)
        {
            Shoot();
        }
    }

    void Shoot()
    {
        // spawn the bullet at the position of firepoint
        GameObject bullet = Instantiate(bulletPrefab, firepoint.position, firepoint.rotation);
        // set the speed of bullet
        Rigidbody2D brb = bullet.GetComponent<Rigidbody2D>();
        brb.velocity = transform.right * speed;
        // if the player is flip, make the bullet speed negative so it goes the opposite way
        if (hasFlip)
        {
            brb.velocity = -transform.right * speed;
        }

    }

}
