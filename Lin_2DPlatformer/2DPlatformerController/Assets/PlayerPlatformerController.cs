﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerPlatformerController : PhysicObject
{
    // speed variable for player and jump action
    public float maxSpeed = 7;
    public float jumpTakeOffSpeed = 7;
    // Text for "Count", "Win", "Weapon Access", and "Count Down"
    public Text countText;
    public Text WinText;
    public Text Weapontext;
    public Text countDown;
    // Import the weapon script to have access for the bool to check if the player has the weapon
    public weapon weaponScript;

    // initiate spriteRenderer and annimator of player
    private SpriteRenderer spriteRenderer;
    private Animator animator;


    // variables that works with text and count down system of the game
    private int count;
    private bool gameFinished;
    private float countdown;
    private int min;
    private int sec;
    private string Sec;


    private void Start()
    {
        // at the beginning of the game, set the time count down and other instance for the game
        weaponScript.hasWeapon = false;
        gameFinished = false;
        countdown = 120f;
        count = 0;
        SetCountText();
    }


    // The function to update the gems that player collect
    void SetCountText()
    {
        countText.text = "Count Gem: " + count.ToString();
    }

    // Use this for initialization
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    protected override void ComputeVelocity()
    {
        // player's controller for moving and jumping
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && grounded)
        {
            velocity.y = jumpTakeOffSpeed;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }
        }


        // bool to check if the current sprite of the player to see if we need to flip the player's sprite by x axis
        bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < 0.01f));
        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
            // when the sprite is fliped, the weapon need to flip as well
            weaponScript.hasFlip = !weaponScript.hasFlip;
            // rotate the weapon 
            weaponScript.firepoint.transform.Rotate(0f, 180f, 0f);
        }

        animator.SetBool("grounded", grounded);
        animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;


        // 00:00 count down form
        if (!gameFinished)
        {
            countdown -= Time.deltaTime;
            min = (int)countdown / 60;
            sec = (int)countdown - min * 60;
            if (sec < 10)
            {

                Sec = "0" + sec.ToString();
            }
            else
            {
                Sec = sec.ToString();
            }
            countDown.text = "Count Down: 0" + min.ToString() + ": " + Sec;

        }

        // when time runs out, game is over and restart the game
        if (countdown < 0)
        {
            WinText.text = "Time ran out";
            gameFinished = true;
            Invoke("RestartLevel", 2f);
        }


    }

    // All the collisions that player has
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // when the player run into the Gate
        if (collision.gameObject.CompareTag("Win Gate"))
        {
            // enough gems collected, win
            if (count == 6)
            {
                WinText.text = "You have escaped from your dream";
                gameFinished = true;
                Invoke("RestartLevel", 2f);

            }
            // remind player to find more gems
            else
            {
                WinText.text = "There are still gems in the map";
                Invoke("SetWinTextToNull", 2f);
            }
        }

        // when the player run into the Gem, collect it
        if (collision.gameObject.CompareTag("Gem"))
        {
            count++;
            SetCountText();
        }

        // when the player run into the fire weapon
        if (collision.gameObject.CompareTag("Fire"))
        {
            // set the weapon bool to true to give the player the access to shoot
            weaponScript.hasWeapon = true;
            collision.gameObject.SetActive(false);
            Weapontext.text = "Now you can press Z to shoot!";
            Invoke("SetWeaponTextToNull", 2f);
        }

        // when the player falls out of the map and hit dead zone
        if (collision.gameObject.CompareTag("Dead End"))
        {
            // change the win text, restart the game
            WinText.text = "You are dead";
            gameFinished = true;
            Invoke("RestartLevel", 2f);
        }

    }

    // function to restart the game
    void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // function to set the win text to null, the purpose is to show text only for 2 seconds
    void SetWinTextToNull()
    {
        WinText.text = "";
    }

    // function to set the weapon text to null
    void SetWeaponTextToNull()
    {
        Weapontext.text = "";
    }


}
